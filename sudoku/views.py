import os
from random import randint

from django.http import HttpResponse
from django.shortcuts import render

from sudoku.utils.helper import decode_output, make_cnf_dimacs


def index(request):
    context = {}
    context["base"] = range(0,16)
    prev_solution=None
    if request.method == 'GET':
        return render(request, 'sudoku.html', context)
    elif request.method == 'POST':
        sudoku_in = []
        operation = request.POST.get("operation",'')
        IS_SOLVE = operation == "solve"
        IS_NEW_SOLUTION = operation == "new_solution"
        if IS_NEW_SOLUTION:
            original_input = request.POST.get("original_input")
            original_input_prc = original_input.split("_")
            original_input_prc.pop()
            original_input_prc = [i.split('-') for i in original_input_prc]
            original_input_dict={}
            for item in original_input_prc:
                row, col, val = item
                original_input_dict[f"{row}_{col}"] = int(val)
            prev_solution = request.POST.get("negated_output", None)
        else:
            original_input =""
            
        base = range(0,16)
        for row in base:
            row_val = []
            for col in base:
                index = f"{row}_{col}"
                if IS_NEW_SOLUTION:
                    cell_val = original_input_dict.get(index,'0')
                else:
                    cell_val = request.POST.get(index,'')
                cell_val = int(cell_val) if cell_val != '' else 0
                row_val.append(cell_val)
            sudoku_in.append(row_val)
            out_file = "out_sudoku.cnf"
        
        marked={}
        for row_index,row in enumerate(sudoku_in):
            for col_index, col in enumerate(row):
                index = f"{row_index}_{col_index}"
                if IS_SOLVE:
                    if col != 0:
                        marked[f"{row_index}-{col_index}"] = col
                        original_input += f"{row_index}-{col_index}-{col}_"
                elif IS_NEW_SOLUTION:
                    if original_input_dict.get(index,False):
                        marked[f"{row_index}-{col_index}"] = col

        context["original_input"] = original_input
        

        #NOTE: modeling solution
        n = make_cnf_dimacs(sudoku_in, out_file, prev_solution=prev_solution)

        #NOTE: solving
        #call sat-solver `minisat`
        sat_output = "sat_sudoku_solution.txt"
        os.system(f"wsl minisat {out_file} {sat_output}")

        #NOTE: decode and print to file
        solved = decode_output(sat_output, n)
        with open("out_sudoku.txt", "r") as f:
            solution = f.read()
        with open(sat_output, "r") as f:
            minisat_output = f.read()
            minisat_output = minisat_output.split('\n')[1]
            negated_output = ' '.join([i if i =='0' else f"-{i}" if '-' not in i else i.replace('-','') for i in minisat_output.split(' ')])
            if prev_solution:
                negated_output = f"{prev_solution}\n{negated_output}"
            context["negated_output"] = negated_output

        # cleanup
        os.remove(out_file)
        os.remove(sat_output)
        os.remove("out_sudoku.txt")
        if solved:
            rows = solution.split("\n")
            solution = [row.strip().split(' ') for row in rows]
            solution.pop()
            solution_marked = []
            for row_index, row in enumerate(solution):
                row_val = []
                for col_index, col in enumerate(row):
                    val=[col,False]
                    if marked.get(f"{row_index}-{col_index}", False):
                        val[1] = True
                    row_val.append(val)
                solution_marked.append(row_val)
            print(solution)
            context["solved"] = solved
            context["message"] = "Input is satisfiable"
            context["solution"] = solution_marked
            return render(request, 'sudoku.html', context)
        else:
            solution = [[[col if col>0 else "",col>0] for col in row] for row in sudoku_in]
            context["message"] = "Input is unsatisfiable"
            context["solution"] = solution
            return render(request, 'sudoku.html', context)
